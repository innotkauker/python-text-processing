#!/usr/bin/env python2
#! coding: utf-8

import json
import nltk
import re
import string
import codecs
import collections
import itertools
import sklearn
from numpy import *
from sklearn import cross_validation
from sklearn.linear_model import Perceptron
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.metrics import make_scorer, f1_score

stopwords = []

class Solution:
    def __init__(self, debug = 2):
        self.debug_level = debug
        self.prepare_stopwords()
        self.prepare_categories('cats.json')
        self.classifiers = [PassiveAggressiveClassifier(n_iter = 100) for i in range(0, len(self.categories))]
        self.cv = TfidfVectorizer(preprocessor = prepare_text, norm = 'l2', analyzer = 'char_wb', decode_error = 'ignore', max_df = 0.90, ngram_range = (1, 8))
        # self.s = RussianStemmer('russian')
        self.marks = {3: 'positive', 2: 'neutral', 1: 'negative'}
        self.texts = []

    def log(self, msg, level = 0):
        if self.debug_level <= level:
            print(msg)

    def prepare_categories(self, path):
        self.categories = json.load(open(path))

    def prepare_training_data(self, data, merge = True):
        self.data = sorted(data, key = lambda i: i['questionId'])
        self.log('total for training: ' + str(len(self.data)), 1)
        if merge:
            groups = itertools.groupby(self.data, lambda i: i['questionId'])
            self.data = []
            for key, value in groups:
                x = list(value)
                e = {'text': x[0]['text'], 'len': len(x), 'answers': reduce(lambda a, b: a + b['answers'], x, x[0]['answers'])}
                if e['answers'] != []:
                    self.data += [e]
            self.log('samples after merge: ' + str(len(self.data)), 1)
        return self.data

    def prepare_stopwords(self):
        global stopwords
        r = re.compile(r'  | ')
        stopwords = []
        for i in codecs.open('stop', 'r', 'utf-8').readlines():
            i = r.split(i)[0]
            if i != '' and i != '\n':
                stopwords += [i]
        stopwords = set(stopwords)

    def gen_y(self, data):
        the_great_y = [[0 for text in range(0, len(data))] for cat in range(0, len(self.categories))]
        for i in range(0, len(data)):
            e = data[i]
            for cat in e['answers']:
                for q in (q[0] for q in self.marks.items() if q[1] in cat):
                    the_great_y[self.categories.index(cat['text'])][i] = q
        return the_great_y

    def train(self, training_corpus):
        data = self.prepare_training_data(training_corpus)
        y = self.gen_y(data)
        X = self.cv.fit_transform([i['text'] for i in data])
        xsum = 0
        for i in range(0, len(self.classifiers)):
            self.classifiers[i].fit(X, y[i])
            # x = cross_validation.cross_val_score(self.classifiers[i], X, y[i], n_jobs = 7, cv = 7, scoring = make_scorer(f1_score, average='micro'))
            # print x
            # xsum += x
        # print "avg: " + str(xsum/13) + ' -> ' + str(sum(xsum/13)/len(xsum))

    def getClasses(self, texts):
        result = []
        X = self.cv.transform(texts)
        res = []
        for c in range(0, len(self.categories)):
            clf = self.classifiers[c]
            res += [clf.predict(X)]
        for text in range(0, len(texts)):
            y = []
            t = [res[c][text] for c in range(0, len(self.categories))]
            for c in range(0, len(self.categories)):
                if t[c] in self.marks:
                    y += [tuple([self.categories[c], self.marks[t[c]]])]
            result += [y]
        return result

def prepare_text(text):
    text = re.sub( r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+  \)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))', ' ', text)
    text = re.sub(r'[0-9]+', '', text)
    text = re.sub(r'\r', ' ', text)
    text = re.sub(r'<br>', ' ', text)

    t = [w for w in nltk.word_tokenize(text) if w not in stopwords]
#    for i in range(0, len(t)):
#        t[i] = stemmer.stem(t[i].lower())
    return ' '.join(t)


if __name__ == '__main__':
    sol = Solution(0)
    sol.train(json.load(open('reviews.json')))
    # exit
    r = sol.getClasses([u'петя жирный, а магазин неплохой.', u'петя жирный, а магазин плохой.', u'деньги заплатил, ничего не дали. как в церковь ходил.'])
    print r
    for i in range(0, len(r)):
        x = r[i]
        for j in x:
            print str(i) + ':' + str(j)
    # sol.prepare_training_data(json.load(open('reviews.json')))
    # sol.texts = [sol.prepare_text(t['text']) for t in sol.data]
    # sol.prepare_estimator_data()
    # sol.cross_validate()
