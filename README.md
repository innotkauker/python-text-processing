# Text processing #

This repository contains the code for two tasks that I had at
CS MSU [text processing course](http://modis.ispras.ru/tpc).

The imlpementation language is python2,
[NLTK](http://www.nltk.org/) and
[scikit-learn](http://scikit-learn.org/) libraries are used.

### Task 1. Opinion characterization system. ###

A script that takes a set of shop reviews grabbed from [Yandex Market](http://market.yandex.ru)
and extracts a set of characteristics from each of them.
The set of possible characteristics is predefined and contains 13 categories,
each with three gradation degrees.
A training set is provided in `reviews.json`.
Input data should have the same structure.
Example of code usage is given in `solution.py`.

### Task 2. Automatic part of speech determination. ###

This script assigns tags representing different parts of speech
of Russian language to words in text. The training set from
[National corpus of Russian language](http://www.ruscorpora.ru) is included.
As in task 1, usage example can be found in the `solutiuon.py` script.

Full description of both tasks in Russian is [avaliable](https://bitbucket.org/innot/python-text-processing/src/master/task-ru-html-cached.htm).
